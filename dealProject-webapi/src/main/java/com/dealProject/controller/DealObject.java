package com.dealProject.controller;

import java.io.Serializable;

public class DealObject implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String dealId;
	private String orderCurrency;
	private String toCurrency;
	private String dealTime;
	private String dealAmount;
	public String getDealId() {
		return dealId;
	}
	public void setDealId(String dealId) {
		this.dealId = dealId;
	}
	public String getOrderCurrency() {
		return orderCurrency;
	}
	public void setOrderCurrency(String orderCurrency) {
		this.orderCurrency = orderCurrency;
	}
	public String getToCurrency() {
		return toCurrency;
	}
	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}
	public String getDealTime() {
		return dealTime;
	}
	public void setDealTime(String dealTime) {
		this.dealTime = dealTime;
	}
	public String getDealAmount() {
		return dealAmount;
	}
	public void setDealAmount(String dealAmount) {
		this.dealAmount = dealAmount;
	}
	
	@Override
	public String toString() {
		String outpt;
		outpt = "Deal Id:::" + this.dealId + "  From Currency:::"+ this.orderCurrency+
				"   To Currency:::" +this.toCurrency + "  deal time:::"+this.dealTime+ "  deal amount:::  ";
		
		return outpt;
	}

}

package com.dealProject.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.dealProject.dao.Deal;
import com.dealProject.dao.DealCount;
import com.dealProject.dao.FileName;
import com.dealProject.intrface.DealInterface;

@Controller
@RequestMapping(value = "/couriernetwork/user")
public class DealController {
	
	Logger logger = LoggerFactory.getLogger(DealController.class);
	
	@Autowired
	DealInterface dealsRepository;
	
	@Autowired
	FileRead fileReadComponent; 

	@ResponseBody
	@RequestMapping(value="/insertdealsfile", method=RequestMethod.POST , consumes= {"multipart/form-data"}  )
	public ResponseObject uploadFile(@RequestParam("file") MultipartFile file) {
		// check if all form parameters are provided
		logger.debug("Entering the method to insert the deals int the DB by the name of file :--" + file.getName());
		String returnValue="Y";
		InputStream is;
		
		ResponseObject rsp = new ResponseObject();
		rsp.setStatus("Y");
		try {
			is = file.getInputStream();
		
			Map<String , List<Deal>> dealmap = null;
			
			dealmap= fileReadComponent.processInputFile(is,file.getOriginalFilename());
		
		
		rsp.setStatus("N");
		FileName fObject = fileReadComponent.getFileNameObject(file, file.getOriginalFilename());
		
		if(dealsRepository.insertFileNameinDB(fObject)  ){
			
			// first insert all the valid deals in the DB
			if(dealmap!=null && dealmap.get("validDeals")!=null){
			dealsRepository.insertAllDeals(dealmap.get("validDeals"));
			rsp.setStatus("Y");
			}
			
			// insert all the invalid deals in the DB
			if(dealmap!=null && dealmap.get("invalidDeals")!=null){
			dealsRepository.insertInvalidDeals(dealmap.get("invalidDeals"));
			}
		}
		
		} catch (IOException e) {
			logger.error("Error occured while reading from the file ::: ", e.getMessage());
		
		}
		return rsp;  
	}
	
	@ResponseBody
	@RequestMapping(value="/getCountofDeals", method=RequestMethod.GET ) 
	public List<DealCount> getCountOfDeals(){
		
		List<DealCount> lstOfDeals = null;
		lstOfDeals= dealsRepository.getCountOfDeals();
		return lstOfDeals; 
	}
	
	@ResponseBody
	@RequestMapping(value="/getCountofDealsbyFileName", method=RequestMethod.POST )
	public List<DealCount> getCountofDealsByFileName(@RequestParam("fileName") String filename){
		
		List<DealCount>lstOfDeals = null;
		lstOfDeals= dealsRepository.countDealsByFileName(filename);
		return lstOfDeals;
	}
	
	@ResponseBody
	@RequestMapping(value="/getlistoffiles" , method=RequestMethod.GET)
	public List<FileName> getListofFileinDB(){
		List<FileName> lstofFiles = null;
		lstofFiles = dealsRepository.getListofAllFile();
		return lstofFiles;
	}
	
}

package com.dealProject.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.dealProject.dao.Deal;
import com.dealProject.dao.FileName;
@Component(value="fileReadComponent")
public class FileRead {
	
	Logger logger = LoggerFactory.getLogger(FileRead.class); 

	public Map<String , List<Deal>> processInputFile(InputStream inputFS,String fileName) {
	    List<Deal> inputList = new ArrayList<Deal>();
	    Map<String , List<Deal>> valid_invalidDeals = new HashMap<String , List<Deal>>();
	    Long time1=System.currentTimeMillis();
	    Long time2 = null;
	    int count=0;
	    String thisLine = "";
	    logger.info("Processing the file @ start time :::" + time1 + "by the file Name:::" + fileName);
	    try{
	      BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
	      // skip the header of the csv
	      List<Deal> validDeals = new ArrayList<Deal>();
		  List<Deal> invaliddeals = new ArrayList<Deal>();
	      while((thisLine = br.readLine()) != null){
	    	  if(count>=1){
	    		  String[] p = thisLine.split(",");
	    		  
	    		  if(validateRecord(p)){
	    			  validDeals.add(mapDealasObject(fileName.toUpperCase(), p));
	    		  }else{
	    			  invaliddeals.add(mapDealasObject(fileName.toUpperCase(), p));
	    		  }
	    		  
	    	  }
	    	  count++;
	      }
	      
	      valid_invalidDeals.put("validDeals", validDeals);
	      valid_invalidDeals.put("invalidDeals", invaliddeals);
	      
	      br.close();
	     
	      time2 = System.currentTimeMillis();
	    } catch (IOException e) {
	    	logger.error("Error occured while mapping the objects and reading from buffer ::: ", e.getMessage());
	    }
	    logger.info("Reading the records of the file ended in::: " + (time2-time1));
	    return valid_invalidDeals ;
	}

	private Deal mapDealasObject(String fileName, String[] p) {
		  Deal itemObj = new Deal();
		  if(p!=null && p.length>=5){
		  itemObj.setUniqueId(null!=p[0] ? p[0]: "" );
		  itemObj.setFromIsoCurrency(null!=p[1] ? p[1]: "");
		  itemObj.setToIsoCurrency(null!=p[2] ? p[2]: "");
		  itemObj.setDealTime(null!=p[3] ? p[3]: "");
		  itemObj.setDealAmount(null!=p[4] ? p[4]: "");
		  itemObj.setDealfileName(fileName);
		  }else{
			  itemObj.setUniqueId( "NA" );
			  itemObj.setFromIsoCurrency("NA");
			  itemObj.setToIsoCurrency( "NA");
			  itemObj.setDealTime( "NA");
			  itemObj.setDealAmount("NA");
			  itemObj.setDealfileName(fileName);
		  }
		  return itemObj;
	}
	
	public FileName getFileNameObject(MultipartFile fileObj, String fileName){
		FileName rtrnObject = new FileName();
		rtrnObject.setSizeOfFile(String.valueOf(fileObj.getSize()));
		rtrnObject.setNameofFile(fileName.toUpperCase());
		return rtrnObject;
	}
	/**
	 * Below method is used to validate a record , By default we are returning as true . 
	 * @param inputArray
	 * @return
	 */
	public boolean validateRecord(String[] inputArray){
		boolean retValue= true;
		
		// returning back from here since the array does not contain the valid fields 
		if(null == inputArray || inputArray.length<5){
			retValue= false;
		}else if (inputArray.length >= 5){
			
			if(StringUtils.isEmpty(inputArray[0].trim()) || StringUtils.isEmpty(inputArray[1].trim()) |StringUtils.isEmpty(inputArray[2].trim()) |StringUtils.isEmpty(inputArray[3].trim())){
				retValue= false;
			}else {
				// we are assuming the from currency and to currency to be of 2 characters 
				if(inputArray[1].trim().length()>2  || inputArray[2].trim().length()>2){
					retValue = false;
				}
				
				// validation for ordering currency 
				Long ordervalue = Long.valueOf(inputArray[4].trim());
				if(ordervalue.doubleValue() <= 0.0 ){
					retValue = false;
				}
				
			}
			
		}
		
		
		return retValue;
	}
	
}

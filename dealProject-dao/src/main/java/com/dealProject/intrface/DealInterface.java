package com.dealProject.intrface;

import java.util.List;

import com.dealProject.dao.Deal;
import com.dealProject.dao.DealCount;
import com.dealProject.dao.FileName;

public interface DealInterface {
	public boolean insertAllDeals(List<Deal> dealObjs);
	
	public boolean insertFileNameinDB(FileName fileName);
	
	public boolean insertInvalidDeals(List<Deal> dealObjs);
	
	public List<DealCount> getCountOfDeals();
	
	public List<DealCount> countDealsByFileName(String fileName);
	
	public List<FileName> getListofAllFile();
}

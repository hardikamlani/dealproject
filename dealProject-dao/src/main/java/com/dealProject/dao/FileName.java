package com.dealProject.dao;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="fileName_table") 
public class FileName {
	
	@Field(value="file_name")
	private String nameofFile;
	
	@Field(value="file_size")
	private String sizeOfFile;
	public String getNameofFile() {
		return nameofFile;
	}
	public void setNameofFile(String nameofFile) {
		this.nameofFile = nameofFile;
	}
	public String getSizeOfFile() {
		return sizeOfFile;
	}
	public void setSizeOfFile(String sizeOfFile) {
		this.sizeOfFile = sizeOfFile;
	}
	
	
}

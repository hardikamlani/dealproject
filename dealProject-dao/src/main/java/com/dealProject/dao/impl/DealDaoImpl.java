package com.dealProject.dao.impl;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
//imports as static
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.dealProject.dao.CollectionDatasource;
import com.dealProject.dao.Deal;
import com.dealProject.dao.DealCount;
import com.dealProject.dao.FileName;
import com.dealProject.docWriter.MongoDocumentWriter;
import com.dealProject.docWriter.MongoSingleHostDocumentWriter;
import com.dealProject.intrface.DealInterface;
import com.dealProject.partition.GridSizeDocumentPartitioner;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Repository(value="dealsRepository")
public class DealDaoImpl implements DealInterface  {
	
	private static final String COLLECTION = "valid_deals";
	
	private static final String COLLECTION_FILENAME="fileName_table";
	
	private static final String COLLECTION_DEALS = "deal_count";
	
	private static int ONCE_WRITE_DOC_SIZE = 70000; 
	
	private Logger logger = LoggerFactory.getLogger(DealDaoImpl.class); 
	
	@Autowired
	MongoTemplate mongoTemplate; 
	
	private MongoDocumentWriter documentWriter;
	
	@Autowired
	private CollectionDatasource collectionDatasource;
	
	@Autowired
	private CollectionDatasource invalidcollectionDatasource;

	@Override
	public boolean insertAllDeals(List<Deal> dealObjs) {
		
		Long time1=System.currentTimeMillis();
		 Long time2 = null;
		 logger.info("Entered in the method to insert the Deal objects in the DB Collection");
		List<DBObject> records = new ArrayList<DBObject>();
       
        documentWriter = new MongoSingleHostDocumentWriter(Deal.class,
                new GridSizeDocumentPartitioner(),
                collectionDatasource,
                4 );
        
        records = new ArrayList<DBObject>();
        long counter = 0;
        for(Deal doc : dealObjs){
        	BasicDBObject bdObj = new BasicDBObject();
        	bdObj.put("deal_UniqId", doc.getUniqueId());
        	bdObj.put("deal_toCurrencyISO",doc.getToIsoCurrency());
        	bdObj.put("deal_fromCurrencyISO", doc.getFromIsoCurrency());
        	bdObj.put("deal_time", doc.getDealTime());
        	bdObj.put("deal_amount", doc.getDealAmount());
        	bdObj.put("deal_filename", doc.getDealfileName());
        	counter++;
        	records.add(bdObj);
        	if(counter % ONCE_WRITE_DOC_SIZE ==0){
        		documentWriter.write(records);
        		records.clear();
        		records = new ArrayList<DBObject>();
        	}
        	
        }
        
        if(records!=null && records.size() >0 ){
        	documentWriter.write(records);
        }
        time2 = System.currentTimeMillis();
        logger.info("Exiting from the method to insert the Deal objects in the DB Collection");
        logger.info("total time taken to insert the records is :::" + (time2-time1));
        
       return true;
	}
	
	

	@Override
	public boolean insertFileNameinDB(FileName fileName) {
		Query query = new Query();
		String nameofFile =""; 
		FileName alreadyExistObject = null;
		boolean returnValue = false;
		if(null!=fileName && fileName.getNameofFile()!=""){
			
			nameofFile = fileName.getNameofFile().toUpperCase();
			query.addCriteria(Criteria.where("file_name").is(nameofFile));
			
			alreadyExistObject = mongoTemplate.findOne(query, FileName.class);
			
			if(null == alreadyExistObject){
			mongoTemplate.insert(fileName, COLLECTION_FILENAME);
			returnValue = true;
			}
		}
		return returnValue;
	}

	@Override
	public List<DealCount> getCountOfDeals() {
		List<DealCount> returnDealList = null;
		logger.info("Enter the Method to get the records of the Countr of the Unique deals in collection");
		Aggregation agg = newAggregation(group("deal_fromCurrencyISO").count().as("deal_count"),project("deal_count").and("deal_fromCurrencyISO").previousOperation());
		AggregationResults<DealCount> groupResults= mongoTemplate.aggregate(agg, COLLECTION, DealCount.class);
		
		returnDealList = groupResults.getMappedResults();
		Query query = new Query();
		for(DealCount dc:returnDealList){
			query.addCriteria(Criteria.where("deal_fromCurrencyISO").is(dc.getDealISOCode()));
			
			DealCount dcClass = mongoTemplate.findOne(query, DealCount.class);
			
			if(dcClass!=null){
				dcClass.setDealISOCode(dc.getDealISOCode());
				dcClass.setDealCount(dc.getDealCount());
				mongoTemplate.save(dcClass);
			}else{
				mongoTemplate.insert(dc, COLLECTION_DEALS);
			}
			
			query = null;
			query =  new Query();
		}
		
		logger.info("Exit the Method to get the records of the Countr of the Unique deals in collection");
		return returnDealList;
	}



	@Override
	public boolean insertInvalidDeals(List<Deal> dealObjs) {
		Long time1=System.currentTimeMillis();
		Long time2 = null;
		logger.info("Entered in the method to insert the invalid Deal objects in the DB Collection");
		List<DBObject> records = new ArrayList<DBObject>();
      
       documentWriter = new MongoSingleHostDocumentWriter(Deal.class,
               new GridSizeDocumentPartitioner(),
               invalidcollectionDatasource,
               4 );
       
       records = new ArrayList<DBObject>();
       long counter = 0;
       for(Deal doc : dealObjs){
       	BasicDBObject bdObj = new BasicDBObject();
       	bdObj.put("deal_UniqId", doc.getUniqueId());
       	bdObj.put("deal_toCurrencyISO",doc.getToIsoCurrency());
       	bdObj.put("deal_fromCurrencyISO", doc.getFromIsoCurrency());
       	bdObj.put("deal_time", doc.getDealTime());
       	bdObj.put("deal_amount", doc.getDealAmount());
       	bdObj.put("deal_filename", doc.getDealfileName());
       	counter++;
       	records.add(bdObj);
       	if(counter % ONCE_WRITE_DOC_SIZE ==0){
       		documentWriter.write(records);
       		records.clear();
       		records = new ArrayList<DBObject>();
       	}
       	
       }
       
       if(records!=null && records.size() >0 ){
       	documentWriter.write(records);
       }
       time2 = System.currentTimeMillis();
       logger.info("Exiting from the method to insert the invalid Deal objects in the DB Collection");
       logger.info("total time taken to insert the records is :::" + (time2-time1));
       
      return true;
	}



	@Override
	public List<DealCount> countDealsByFileName(String fileName) {
		List<DealCount> returnDealList = null;
		logger.info("Enter the Method to get the records of the Countr of the Unique deals in collection");
		Aggregation agg = newAggregation( match(Criteria.where("deal_filename").is(fileName.toUpperCase())),group("deal_fromCurrencyISO").count().as("deal_count"),project("deal_count").and("deal_fromCurrencyISO").previousOperation());
		AggregationResults<DealCount> groupResults= mongoTemplate.aggregate(agg, COLLECTION, DealCount.class);
		
		returnDealList = groupResults.getMappedResults();
		
		logger.info("Exit the Method to get the records of the Countr of the Unique deals in collection");
		return returnDealList;
	}



	@Override
	public List<FileName> getListofAllFile() {
		List<FileName> flnameList = null;
		flnameList = mongoTemplate.findAll(FileName.class, "fileName_table");
		return flnameList;
	}
	

}

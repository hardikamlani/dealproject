package com.dealProject.dao;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="count_deals")
public class DealCount {
	
	@Field(value="deal_fromCurrencyISO")
	private String dealISOCode;
	
	@Field(value="deal_count")
	private String dealCount;

	public String getDealISOCode() {
		return dealISOCode;
	}

	public void setDealISOCode(String dealISOCode) {
		this.dealISOCode = dealISOCode;
	}

	public String getDealCount() {
		return dealCount;
	}

	public void setDealCount(String dealCount) {
		this.dealCount = dealCount;
	}
	
	
	
	
}

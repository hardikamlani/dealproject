package com.dealProject.dao;


import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
public class CollectionDatasource {

    private  DBCollection dbCollection;
    
    private static final String COLLECTION = "valid_deals";
	
	public CollectionDatasource(MongoTemplate mongTemplate , String collectionName) {
            this.dbCollection = mongTemplate.getCollection(collectionName);
          //  this.dbCollection.remove( new BasicDBObject() );

            this.dbCollection.createIndex( new BasicDBObject( "_id", 1 ) );
    }

    public DBCollection getCollection() {
        return this.dbCollection;
    }
    
}

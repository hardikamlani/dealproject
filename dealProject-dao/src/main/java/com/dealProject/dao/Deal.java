package com.dealProject.dao;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.mongodb.ReflectionDBObject;

@Document(collection="valid_deals") 
public class Deal extends ReflectionDBObject implements Serializable { 
	
	@Field(value="deal_UniqId")
	private String uniqueId;
	
	@Field(value="deal_fromCurrencyISO")
	private String fromIsoCurrency;
	
	@Field(value="deal_toCurrencyISO")
	private String toIsoCurrency;
	
	@Field(value="deal_time")
	private String dealTime;
	
	@Field(value="deal_amount")
	private String dealAmount;
	
	@Field(value="deal_filename")
	private String dealfileName;
	
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getFromIsoCurrency() {
		return fromIsoCurrency;
	}
	public void setFromIsoCurrency(String fromIsoCurrency) {
		this.fromIsoCurrency = fromIsoCurrency;
	}
	public String getToIsoCurrency() {
		return toIsoCurrency;
	}
	public void setToIsoCurrency(String toIsoCurrency) {
		this.toIsoCurrency = toIsoCurrency;
	}
	public String getDealTime() {
		return dealTime;
	}
	public void setDealTime(String dealTime) {
		this.dealTime = dealTime;
	}
	public String getDealAmount() {
		return dealAmount;
	}
	public void setDealAmount(String dealAmount) {
		this.dealAmount = dealAmount;
	}
	public String getDealfileName() {
		return dealfileName;
	}
	public void setDealfileName(String dealfileName) {
		this.dealfileName = dealfileName;
	}
	
}

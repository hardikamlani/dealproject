package com.dealProject.docWriter;


import com.mongodb.DBObject;

import java.util.List;

public interface MongoDocumentWriter {

    public void write ( List<DBObject> documents );
}

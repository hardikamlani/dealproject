/**
 * 
 */
var conObj =angular.module('PTMClientApp.ComonConstants',[]);

conObj.constant('CommonConstants',{
	
	'FLG_ACTV' : 'Y',
	'FLG_DEACTV' :'N',
	'ERR_STNSELC' :'Please Select a Station',
	'SUCC_MAPDRULTOSTN':'Station(s) mapped with Rule Successfully',
	'ERR_ONESRCHCRIT' :'Please select atleast one search criteria',
	'SUCC_CHNGDRULSETN' :'Rule settings Saved Successfully',
	'SUCC_CHNGDNOTFSETTN' :'Notification settings Saved Successfully',
	'SUCC_NEWUSRCRT':'User Saved Successfully',
	'ERR_RECALEXST':'Record already exists',
	'ERR_PLSSELCTUSRGRP':'Please select a usergroup',
	'ERR_INVALIDUSR':'Invalid User',
	'SUCC_USRDELTSUCC':'User deleted Successfully',
	'ERR_PLZSELCTTODELT':'Please select atleast one user to delete',
	'ERR_BLNKINCOMUSRID':'Blank/Incomplete UserId.UserId Format : S[6 Digit UserId]',
	'SUCC_SVDUSRGGRP':'User Group saved successfully',
	'ERR_GRPNMEXIST':'Group Name already exists please specify another name',
	'SUCC_GRPDTLUP':'Group Details Updated Successfully',
	'SUCC_DLTUSRGRP':'Group deleted Successfully',
	'ERR_SLCTUSRGRPDELT':'Please Select a group to delete',
	'SUCC_SVDPERMSUCC':'Permissions Saved successfully',
	'ERR_SELCTPERM':'Please Select a Permission',
	'ERR_FRMTODT':'Please Select both(FROM and TO) dates',
	'SUCC_SNTERMSG':'Message sent Successfully',
	'ERR_SENDMSG':'Error occured while sending',
	'ERR_PLZSELCTFLGHT':'Please select atleast one flight',
	'ERR_SENDMSGLENGTH':'Cannot Send Message , Length Exceeded',
	'ERR_PLZSELCTPASSNGR':'Please Select atleast one Passengar',
	'REQ_TIMEOUT' : 80,
	'REQ_TIMEOUT_ERRMSG':'Messaging Engine is Busy.Please Try Again Later',
	'ERR_REGEX':'[.,:-/]special characters and Alphanumeric are allowed',
	'ERR_INCORRECTTIMPARAM':'Time Parameter should be in order Gate Assign > Gate Change > Go to Gate',
	'RC_GOTOGATE':'GOTOGAT',
	'RC_GATECHANGE':'GATCHG',
	'RC_GATEASSIGN':'GATASSG'
	
	
	
});

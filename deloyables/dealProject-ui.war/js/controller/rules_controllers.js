var pteConrollersObj = angular.module('DealApp.controllers', []);

var pteControllers = {};
pteControllers.globalPgCntrl = function($scope, $http, $rootScope, RuleConfigServices , $timeout) {

$scope.pageTitle="Deal project";
$scope.successVisible = false;
$scope.uploadFile = function(){
    var file = $scope.myFile;
    $scope.successVisible = false;
    console.log('file is ' );
    console.dir(file);
    RuleConfigServices.uploadFile(file).success(
	function(data){
    	
		if(data.status=="Y"){
    	$scope.successVisible = true;
		
		
		$scope.respmessage = "insert file is success"
    	
    	$timeout( function(){$scope.successVisible = false;}, 3000);
		
		}else{
			$scope.successVisible = true;
			$scope.respmessage = "File already exists"
    	
    	$timeout( function(){$scope.successVisible = false;}, 3000);
		
		}
    	
    }
	).error(function(data){
		
	})
		
	;
 };
};


pteControllers.rulePgCntrl = function($scope,$http,RuleConfigServices,$rootScope,$timeout){
	
	
	$scope.getFilelist = function(){
		
		RuleConfigServices.getlistofFiles().success(
				
				function(data){
					$scope.filelist = data;
				}
				
		).error(
		
		);
		
	}
	
	$scope.getFilelist();
	$scope.fileData = [];
	
	$scope.applyruleSearchFilter= function(rule){
		
		if((rule === undefined) || (rule.stationfilterValue == "" && rule.rulefilterValue =="" 
		  &&  rule.rulestatusfilterValue == "")){
		
		
		}
			
		RuleConfigServices.getCountofDealsbyFileName(rule.rulefilterValue).success(
			function(data){
				$scope.fileData = data;
			}	
		).error(
			function(data){
				
			}
				
		);
		
	}		
		
};

pteConrollersObj.controller(pteControllers);



/**
 * this File Contains data of all controllers pertainig to rule ,user,message screen
 */
var pteConrollersObj = angular.module('PTMClientApp.UserControllers',[]);

var pteControllers ={};

// user management page controller
pteControllers.userMngtPgCntrl = function($scope,$http,UserMngmtServices,$rootScope, ngDialog,
		$timeout,ngTableParams,$filter,CommonConstants,$location){ 
	
	$scope.tab =0;
	
	$scope.usrsrch ={};
	
	$scope.usrsrch={
			flagActive :CommonConstants.FLG_ACTV,
			staffName : "",
			staffId :""
	};
	
	$scope.applyCheckBox= function(){
			var userIdList=[];
			angular.forEach($scope.srchcritList , function(lstuser){
				if(lstuser.isChecked)
					userIdList.push(lstuser.userId);
			});
			return userIdList;
	};
	
	$scope.saveNewUser = function(userObj){
	
		
		var userGrupList =[]; 
		var ctusr = false;
		var chkusr = $filter('filter')($scope.usersrchList ,  {'userId':userObj.userId }); // $scope.validateExistingUser(userobj); //
		
		if(chkusr.length>0 && $scope.popupst=='C')
			 ctusr= false;
		else if(chkusr.length <=0 && $scope.popupst=='C')
			 ctusr= true;
		else if($scope.popupst=='E')
			 ctusr=true;

		angular.forEach($scope.userGroupCheckBoxList , function(groupObj){
			if(groupObj.isCheckd === true){
				userGrupList.push(groupObj.groupId);
			}
		});
		
		$scope.user.luUser=$scope.loggedUserName;
		$scope.user.luDate=new Date();	
		$scope.user.usergrpsList = userGrupList;
		$scope.user.luUser =$scope.loggedInUserId;
		$scope.user.luDate = new Date();
		if($scope.validuser == 'T'){
		if($scope.user.usergrpsList.length>0){
		if(ctusr){
		UserMngmtServices.savenewUser($scope.user).success(
				function(data){					
				   $scope.userInfoMsg =true;
				   $scope.setoperationMessage(CommonConstants.SUCC_NEWUSRCRT,1);
                   $timeout( function(){$scope.userInfoMsg = false;}, 3000);				   
					if($scope.tableParams!=null){
						$scope.tableParams.$params.count = 0; 
					} 
					$scope.readUserList();
					$scope.searchUserbyCrit($scope.usrsrch);
					ngDialog.close();
					
				}
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}
		);
		}else{
		$scope.userInfoMessage = true;
			$scope.setoperationMessage(CommonConstants.ERR_RECALEXST,2);
			 $timeout( function(){$scope.userInfoMessage = false;}, 3000);	
		}
		}else{
			$scope.userInfoMessage = true;
			$scope.setoperationMessage(CommonConstants.ERR_PLSSELCTUSRGRP,2);
			 $timeout( function(){$scope.userInfoMessage = false;}, 3000);	
		}
		}else{
			$scope.userInfoMessage = true;
			$scope.setoperationMessage(CommonConstants.ERR_INVALIDUSR,2);
			
			 $timeout( function(){$scope.userInfoMessage = false;}, 3000);
		}
		
	};
	
	$scope.departmentlist = function(){
		UserMngmtServices.readDepartmentlist().success(
				function(data){
				$scope.departmntList = data;	
				}
				
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}
		);
		
	};
	
	
	$scope.readUserList = function(){
		UserMngmtServices.readUserList().success(
				function(data){
				$scope.usersrchList = data;	
				
				if($scope.tableParams!=null){
					$scope.tableParams.$params.count = 0; 
				}  
				
				$scope.tableParams = new ngTableParams({
					page : 1, // show first page
					count : 5// count per page
				}, {
					total : data.length, // length of data
					getData : function($defer, params) {
						$defer.resolve(data.slice((params.page() - 1)
								* params.count(), params.page()
								* params.count()));
					}
				}); 
				
				
				}
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}	
		);
	};
	
	$scope.getDeptName = function(dptId){
		
		$scope.dt = $filter('filter')($scope.departmntList ,  {'deptId':dptId || undefined });
		
		return $scope.dt!=undefined? $scope.dt[0].deptName:'';
		
	};
	
	$scope.editUser= function(userObj){
		$scope.popupst='E';
		$scope.user={};
		$scope.user.userId = userObj.userId;
		$scope.user.userName = userObj.userName;
		$scope.user.departmentId = userObj.departmentId;
		$scope.user.flagActive = userObj.flagActive;
		$scope.user.luUser =$scope.loggedInUserId;
		$scope.user.luDate = new Date();
		$scope.validuser= "T";
		var grparray = userObj.usergrpsList;
		
		$scope.tab=1; 
		
		angular.forEach($scope.userGroupCheckBoxList, function(groupObj){
			
			if(grparray.indexOf(groupObj.groupId)>-1)
				groupObj.isCheckd = true;
			else
				groupObj.isCheckd = false; 
		});   
	};
	
	$scope.deleteUser = function(){
		var lst =$scope.applyCheckBox();
		
		if(lst.length>0){
		UserMngmtServices.deleteUsers(lst).success( 
				function(data){			
				$scope.userInfoMsg = true;
			   $scope.setoperationMessage(CommonConstants.SUCC_USRDELTSUCC,1);
				 $timeout( function(){$scope.userInfoMsg = false;}, 3000);
				if($scope.tableParams!=null){
					$scope.tableParams.$params.count = 0; 
				} 
				$scope.readUserList();
				
				if(lst.indexOf($scope.loggedInUserId)>=0){
					UserMngmtServices.logoutUser().success().error();
					$timeout(function(){sessionStorage.clear();window.open('../client/logout.html','_self');},4000);
				}else{
				$scope.initsrch=true;
				$scope.searchUserbyCrit($scope.usrsrch);
				}
				}
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}
		);
		}
		else{
			$scope.userInfoMsg = true;
			$scope.setoperationMessage(CommonConstants.ERR_PLZSELCTTODELT,2);
				 $timeout( function(){$scope.userInfoMsg = false;}, 3000);
		}
	};
		
	$scope.readAllUserGroups = function(){
		UserMngmtServices.readAllUserGroups().success(
				function(data){
				$scope.userGroupCheckBoxList = data;	
				}
		).error(
				
		);
	};
	
	$scope.opencreateUserpopup = function() {
	$scope.userInfoMessage = false;
		
		if($scope.popupst == 'C')
			$scope.resetusergpList(); 
		
		ngDialog.open({
			scope : $scope,
			template : 'addUserDialogBox',
			className : 'ngdialog-theme-default'
		});
	};
	
	$rootScope.$on('ngDialog.opened', function(e, $dialog) {
		
	});

	$rootScope.$on('ngDialog.closed', function(e, $dialog) {
		
	});

	$rootScope.$on('ngDialog.closing', function(e, $dialog) {
		
		$scope.employList=[];
	});
	
	$scope.resetSearchUser= function(){
	
		$scope.usrsrch={};
		$scope.usrsrch={
				flagActive :CommonConstants.FLG_ACTV,
				staffName : "",
				staffId :""
		};
		$scope.searchUserbyCrit($scope.usrsrch);
	};
	
	$scope.applyAllCheckBox = function(aplValue){
		angular.forEach($scope.srchcritList , function(lstuser){
			lstuser.isChecked = aplValue;
		});

	};
	
	$scope.openEmployeeBox = function(invalue){
	
		if(invalue==1){
		$scope.srchtype ='I';
	
		     if(!($scope.user.userId == undefined) && !($scope.user.userId == '') && !($scope.user.userId == 's')&& ($scope.popupst =='C') ){
			    $scope.usrdb ={}; 
			    $scope.usrdb.staffId= $scope.user.userId;
			    $scope.searchEmployee($scope.usrdb,$scope.srchtype);
		     }else if($scope.popupst =='C'){
		    	$scope.employList=[];
		    	$scope.shownodata = false;
		    	$scope.usrdb ={}; 
		     }
		}
		
		if(invalue==2){
		$scope.srchtype = CommonConstants.FLG_DEACTV;	
		
		if(!($scope.user.userName == undefined) && !($scope.user.userName == '') && ($scope.popupst =='C') ){
		    $scope.usrdb ={}; 
		    $scope.usrdb.staffName= $scope.user.userName;
		    $scope.searchEmployee($scope.usrdb,$scope.srchtype);
	     }else if($scope.popupst =='C'){
	    	$scope.employList=[];
	    	$scope.shownodata = false;
	    	$scope.usrdb ={}; 
	     }
		
		
		}
		
		  
	if($scope.popupst =='C'){
		ngDialog.open({
			scope : $scope,
			template : 'searcEmployeeDilgBox',
			className : 'ngdialog-theme-default'
		}); 
	} 
	
	
	};
	
	
	$scope.searchEmployee = function(userDB,srchtyp){
		 var tempvar ;
		
		if(srchtyp =='I')
			tempvar = userDB.staffId;
		if(srchtyp ==CommonConstants.FLG_DEACTV)
			tempvar = userDB.staffName;
		if(srchtyp =="IN")
			tempvar = userDB;
		
		UserMngmtServices.getEmployee(tempvar , srchtyp ).success( 
				function(data){
					$scope.employList= data;
					$scope.shownodata=($scope.employList.length >0 ? true : false );
					if($scope.employeetable!=null){
						$scope.employeetable.$params.count = 0; 
					}
					
						$scope.employeetable  = new ngTableParams({
							page : 1, // show first page
							count : 5
						// count per page
						}, {
							total : $scope.employList.length, // length of data
							getData : function($defer, params) {
								$defer.resolve($scope.employList.slice((params.page() - 1)
										* params.count(), params.page()
										* params.count()));
							}
						});
					
				}
				
				
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}
		);
		
	};
	
	$scope.setUserDetailstoCreate = function(userdtls){
		
		$scope.user={};
		$scope.user.userId = userdtls.userId;
		$scope.user.userName = userdtls.userName;
		$scope.popupst='C';
		ngDialog.close();
		$scope.opencreateUserpopup(); 
	};
	
	$scope.srchUseronBlur= function(srchType ,value){
		
		if(srchType==1 && !(value == undefined) && !(value == '') && !(value == 's')&& ($scope.popupst =='C') ){
			
			UserMngmtServices.getEmployee(value , "I" ).success(
					function(data){
						if(data.length==0 || data.length >1)
							$scope.validuser = "F";
						else
							$scope.validuser= "T";
					}
					
			).error(
					
			);
		
		}
		
		if(srchType==2 && !(value == undefined) && !(value == '') && ($scope.popupst =='C') ){  
			
		}
		
	};
	
	$scope.resetusergpList= function(){
		
		angular.forEach($scope.userGroupCheckBoxList, function(groupObj){
			groupObj.isCheckd = false; 
		});
		
	};
	
	$scope.handleSelectAllCb = function(ipvl){
		
		if(!ipvl){
		var stlist;
		stlist =  $filter('filter')($scope.srchcritList ,  {'isChecked':false});
		if(stlist.length != $scope.srchcritList.length){
			$scope.allcbValue = false;
		}
		
		}
		
		
		if(ipvl){
			
			var st1list;
			st1list =  $filter('filter')($scope.srchcritList ,  {'isChecked':true});
			if(st1list.length == $scope.srchcritList.length)
				$scope.allcbValue = true;
		}
		
	};
	
	$scope.searchUserbyCrit = function(usrsrch){
		
		if(null!=usrsrch && ((usrsrch.staffId == "") && (usrsrch.staffName == "") && (usrsrch.departmentId =="") && (usrsrch.flagActive == ""))){ 
			 $scope.userInfoMsg = true;
			 $scope.setoperationMessage(CommonConstants.ERR_ONESRCHCRIT,2);
			 $timeout( function(){$scope.userInfoMsg = false;}, 3000);
			// $scope.initsrch = true;
			
		}else if(null!=usrsrch && (!(usrsrch.staffId == "") && ((usrsrch.staffId == "S") ||  (usrsrch.staffId.substring(0,1)!="S")))){  
			
			 $scope.userInfoMsg = true;
			 $scope.setoperationMessage(CommonConstants.ERR_BLNKINCOMUSRID,2);
			 $timeout( function(){$scope.userInfoMsg = false;}, 3000);
			// $scope.initsrch = true;
			
		}else{
			
			UserMngmtServices.searchUser(usrsrch).success(
					function(data){
									$scope.srchcritList = data;	
									$scope.initsrch = false;
					
									if($scope.srchtableParams!=null){
											$scope.srchtableParams.$params.count = 0; 
									}  
					
									$scope.srchtableParams = new ngTableParams({
											page : 1, // show first page
											count : 5// count per page
									}, {
											total : data.length, // length of data
											getData : function($defer, params) {
												$defer.resolve(data.slice((params.page() - 1)
														* params.count(), params.page()
														* params.count()));
										}
									}); 
					
					}	
			).error(
					function(data,status,headers,config){
						$scope.errorRedirection(data, status, headers, config);
					}
					
			);	
		}
	};
	
	$scope.lookupEmployee = function(usrdb){
		
		if(usrdb!= undefined && usrdb!=null ){ 
		
			if(!(usrdb.staffId == undefined) && !(usrdb.staffId == '') && !(usrdb.staffId == 's') && ((usrdb.staffName == undefined) || (usrdb.staffName == ''))){
				$scope.searchEmployee(usrdb,"I");
			}
			else if(!(usrdb.staffName == undefined) && !(usrdb.staffName == '') && ((usrdb.staffId == undefined) || (usrdb.staffId == '') || (usrdb.staffId == 's') ) )
				$scope.searchEmployee(usrdb,CommonConstants.FLG_DEACTV);
			else
				$scope.searchEmployee(usrdb,"IN");
			
		}
	};
	
	$scope.resetCrtUser = function(){
		
		$scope.user.userId = "";
		$scope.user.userName ="";
		$scope.user.flagActive ="";
		$scope.user.departmentId ="";
		$scope.user.userType = "";
		$scope.user.designation ="";
		$scope.user.email ="";
		$scope.resetusergpList();
		
	};
	
	$scope.confirmDeleteUser = function(){
		
		var lst =$scope.applyCheckBox();
		
		if(lst.length>0){
			
			ngDialog.open({
				scope : $scope,
				template : 'confirmDeleteUser',
				className : 'ngdialog-theme-default'
			}); 
			
		}else{
			$scope.userInfoMsg = true;
			$scope.setoperationMessage(CommonConstants.ERR_PLZSELCTTODELT,2);
			$timeout( function(){$scope.userInfoMsg = false;}, 3000);
		}
		
	};
	
/*	$scope.validateExistingUser = function(userObj){
		
		if(userObj.userId != ""){
			usrsrch ={};
			usrsrch.staffId = userObj.userId;
		}
		
		var result=[];
		if($scope.validuser == 'T'){
		UserMngmtServices.searchUser(usrsrch).success(
				function(data){
					result = data;
				}
		).error();
		}
		
		return result;
	}; */
	

	
		$scope.$watch('usersuccess', function(){
			if($rootScope.usersuccess == 'Y'){
				UserMngmtServices.readDepartmentlist().success(
						function(data){
						$scope.departmntList = data;
						$scope.readUserList();
						$scope.searchUserbyCrit($scope.usrsrch);	
						$scope.readAllUserGroups();
						}
						
				).error(
						function(data,status,headers,config){
							$scope.errorRedirection(data, status, headers, config);
						}
				);
			}else if($rootScope.usersuccess == 'N')
				$location.path('/accessDenied');
		});
		
	$scope.changeselectIndexbc(2,'Manage User');
};

pteControllers.userGrpPermMngmtCntrl = function($scope,$http,UserMngmtServices,$timeout,$location,CommonConstants,$location,$rootScope){
	
	$scope.permissionmap={};
	
	//read all user groups method.
	$scope.readAllUserGroups = function(){
		UserMngmtServices.readAllUserGroups().success(
				function(data){
				$scope.userGroupDropdown = data;	
				
				}
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}		
		);
	};
	
	// read the user permission list.
	$scope.readAllUserPermissions = function(){
		UserMngmtServices.readAllPermissions().success(
				function(data){
				$scope.userPermissionsList = data;	
				
				angular.forEach($scope.userPermissionsList, function(userPermission){
					userPermission.isChecked = false; 
				});
				
				}
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}
		);
	};
	
	
	//read userpermisson to a specific group
	$scope.readUserGrouppermission = function(){
		
		var reqObj=$scope.userGroupSelected;
		
		$scope.applyAllCheckBoxPrmsn(false);
		$scope.inptvalue = false;
		
		if(reqObj!=null && reqObj!=undefined){
		UserMngmtServices.readAllUserGroupPermission(reqObj).success(
				function(data){
						$scope.permissionmap=data;
						$scope.applyCheckBox();
				}
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}
		);
		}
	};
	
	
	//method used to save the userpermission
	$scope.saveuserPermission = function(){
		
		var respObj=[];
		
		if(!($scope.userGroupSelected == null)){
		
		angular.forEach($scope.userPermissionsList , function(userPermission){
			if(userPermission.isChecked === true){
				respObj.push(userPermission.permissionId);
			}
		});
		
		if(respObj.length>0){
		UserMngmtServices.mappermissiontoUserGroup($scope.userGroupSelected,respObj).success(
				function(data){
					
					$scope.permissionInfoMsg = true;
			     $scope.setoperationMessage(CommonConstants.SUCC_SVDPERMSUCC,1);
				 $timeout( function(){$scope.permissionInfoMsg = false;}, 3000);
				
				}
					
			).error(
					function(data,status,headers,config){
						$scope.errorRedirection(data, status, headers, config);
					}
			);
		}else{
		$scope.permissionInfoMsg = true;
			$scope.setoperationMessage(CommonConstants.ERR_SELCTPERM,2);
				 $timeout( function(){$scope.permissionInfoMsg = false;}, 3000);
			
		} 
		
		}else{
			$scope.permissionInfoMsg = true;
			$scope.setoperationMessage(CommonConstants.ERR_PLSSELCTUSRGRP,2);
				 $timeout( function(){$scope.permissionInfoMsg = false;}, 3000);
			
		}
		
	};
	
	//method used to apply the permissions to usergroup
	$scope.applyCheckBox= function(){
		angular.forEach($scope.userPermissionsList, function(userPermission){
			if($scope.permissionmap[userPermission.permissionId]===undefined){
			userPermission.isChecked = false; 
			}else{
				userPermission.isChecked = true;
			}
		});
		
	};
	
	
	$scope.applyAllCheckBoxPrmsn = function(aplValue){
		angular.forEach($scope.userPermissionsList , function(userPermission){
			userPermission.isChecked = aplValue;
		});

	};
	
	$scope.resetuserpermissions = function(){
		$scope.userGroupSelected=null;
		$scope.applyAllCheckBoxPrmsn(false);
		$scope.inptvalue = false;
	};
	$scope.$watch('usersuccess', function(){
		if($rootScope.usersuccess == 'Y'){
		UserMngmtServices.readAllPermissions().success(
				function(data){
				$scope.userPermissionsList = data;	
				
				angular.forEach($scope.userPermissionsList, function(userPermission){
					userPermission.isChecked = false; 
				});
				$scope.readAllUserGroups();
				
				}
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}
		);
		
		
		}else if($rootScope.usersuccess == 'N')
			$location.path('/accessDenied');
	});
	
	
	$scope.changeselectIndexbc(2,'Set Permission'); 
};

pteControllers.userGrpMngmtCntrl = function($scope,$http,UserMngmtServices,ngTableParams,$rootScope, ngDialog,
		$timeout,$filter,CommonConstants,$location) {
	
	$scope.saveUserGroup = function(userGrpObj){
		
		userGrpObj.luUser =$scope.loggedInUserId;
		userGrpObj.luDate = new Date();
		
		
		/*UserMngmtServices.validateUsergroupbyName(userGrpObj.groupName).success(	
				function(data){
					$scope.chkusrgrp = data;
				}
		).error(
				function(data){}
		);*/
		
		$scope.chkusrgrp=$filter('filter')($scope.userGroupList ,  {'groupName':userGrpObj.groupName || undefined },true);
		
		if($scope.chkusrgrp.length<=0){ 
		
		UserMngmtServices.saveUserGroup(userGrpObj).success(
				
				function(data){
				$scope.userGrpInfoMsg =true;
					$scope.setoperationMessage(CommonConstants.SUCC_SVDUSRGGRP,1);
					$timeout(function() {
						$scope.userGrpInfoMsg = false;
					}, 3000);
					if($scope.tableParams!=null){
						$scope.tableParams.$params.count = 0; 
					}
					$scope.readAllUserGroups();
					
					ngDialog.close();
				}
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}
		);
		

				} else {

			$scope.usrGrpMsg = true;
			$scope.setoperationMessage(
					CommonConstants.ERR_GRPNMEXIST, 2);
			$timeout(function() {
				$scope.usrGrpMsg = false;
			}, 3000);

		}
	};
	
	$scope.editUsergroup = function(userGrpObj){
		userGrpObj.luUser =$scope.loggedInUserId;
		userGrpObj.luDate = new Date();
		
		var oldUsergrpObj = $filter('filter')($scope.userGroupList ,{'groupId' :userGrpObj.groupId} );
		
		if(oldUsergrpObj[0].groupName.toUpperCase() == userGrpObj.groupName.toUpperCase()){
			$scope.editchkusrgrp =[];
		}else{
			$scope.editchkusrgrp = $filter('filter')($scope.userGroupList ,  {'groupName':userGrpObj.groupName || undefined },true);
		}
		 
		if($scope.editchkusrgrp.length<=0){ 
		UserMngmtServices.saveUserGroup(userGrpObj).success(
				function(data){
					
					if($scope.tableParams!=null){
						$scope.tableParams.$params.count = 0; 
					}
					$scope.readAllUserGroups();
					$scope.userGrpInfoMsg =true;
					$scope.setoperationMessage(CommonConstants.SUCC_GRPDTLUP,1);
				 $timeout( function(){$scope.userGrpInfoMsg = false;}, 3000);
					ngDialog.close();
				}
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}
		);
		}else{
			$scope.usrGrpMsg = true;
			$scope.setoperationMessage(
					CommonConstants.ERR_GRPNMEXIST, 2);
			$timeout(function() {
				$scope.usrGrpMsg = false;
			}, 3000);
			
		} 
		
	};
	
	//read all user groups method.
	$scope.readAllUserGroups = function(){
		UserMngmtServices.readUserGroupsInfo().success(
				function(data){
				$scope.userGroupList = data;    
				
				$scope.tableParams = new ngTableParams({
					page : 1, // show first page
					count : 5// count per page
				}, {
					total : data.length, // length of data
					getData : function($defer, params) {
						$defer.resolve(data.slice((params.page() - 1)
								* params.count(), params.page()
								* params.count()));
					}
				});
				
				}
		).error(
				function(data,status,headers,config){
			$scope.errorRedirection(data, status, headers, config);
				}
				
		);
	};
	
	$scope.resetUsergroupInputs = function(){
		
		usergrp.groupName ="";
		usergrp.flagActive="";
	};
	
	$scope.deleteUserGroup = function(){
		
		var usergrpIdList = $scope.applyCheckBox();
		
		if(usergrpIdList.length>0){
		UserMngmtServices.deleteUserGroup(usergrpIdList).success( 
				function(data){				
			
				if($scope.tableParams!=null){
					$scope.tableParams.$params.count = 0; 
				}
					$scope.readAllUserGroups();
					$scope.userGrpInfoMsg =true;
					$scope.setoperationMessage(CommonConstants.SUCC_DLTUSRGRP,1);
				 $timeout( function(){$scope.userGrpInfoMsg = false;}, 3000);
				}
		).error(
				function(data,status,headers,config){
					$scope.errorRedirection(data, status, headers, config);
				}
		);
		}else{
			
			
			     $scope.userGrpInfoMsg =true;
				 $scope.setoperationMessage(CommonConstants.ERR_SLCTUSRGRPDELT,2);
				 $timeout( function(){$scope.userGrpInfoMsg = false;}, 3000);
			
		}
	};
	
	$scope.applyCheckBox= function(){
	
			var usergrpIdList=[];
			angular.forEach($scope.userGroupList , function(userGroup){
				if(userGroup.isChecked)
				usergrpIdList.push(userGroup.groupId);
			});
			
			return usergrpIdList;
	};
	
	$scope.applyAllCheckBox = function(aplValue){
		angular.forEach($scope.userGroupList , function(userGroup){
			userGroup.isChecked = aplValue;
		});

	};
	
	$scope.resetUsergroupcretBox = function(){
		
		usergrp.groupName ="";
		usergrp.flagActive =""; 
	};
	
	$scope.openeditUsergrppopup = function(inputObj) {
		$scope.popupst="E"; 
		$scope.usergp={};
		$scope.usergp.groupId = inputObj.groupId;
		$scope.usergp.groupName = inputObj.groupName;
		$scope.usergp.flagActive = inputObj.flagActive;
		$scope.openUserGroupPopup();
		
	};
	
	$scope.openUserGroupPopup = function(){
		
		ngDialog.open({
			scope : $scope,
			template : 'editUsergroupDialogBox',
			className : 'ngdialog-theme-default'
		});
	};
	
	
	$rootScope.$on('ngDialog.opened', function(e, $dialog) {
	
	});

	$rootScope.$on('ngDialog.closed', function(e, $dialog) {
	
	});

	$rootScope.$on('ngDialog.closing', function(e, $dialog) {
	
	});
	
	$scope.confirmDeleteUserGroup = function(){
		
		var lst =$scope.applyCheckBox();
		
		if(lst.length>0){
			
			ngDialog.open({
				scope : $scope,
				template : 'confirmDeleteUserGroup',
				className : 'ngdialog-theme-default'
			}); 
			
		}else{
			$scope.userGrpInfoMsg =true;
			 $scope.setoperationMessage(CommonConstants.ERR_SLCTUSRGRPDELT,2);
			 $timeout( function(){$scope.userGrpInfoMsg = false;}, 3000);
		}
		
	};
	
	$scope.$watch('usersuccess', function(){
		if($rootScope.usersuccess == 'Y'){
			$scope.readAllUserGroups();
		}else if($rootScope.usersuccess == 'N')
			$location.path('/accessDenied');
	});

	
	$scope.changeselectIndexbc(2,'Create User Group');
	
};


pteConrollersObj.controller(pteControllers); 
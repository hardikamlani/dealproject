/**
 * main application module 
*/
var pteRouter = angular.module('DealApp', ['ngRoute','DealApp.controllers','DealApp.services']);  
 
 // router config to show rule , communication and user management screen after assigining them respective controllers. 
 pteRouter.config(['$routeProvider', function($routeProvider){
	
	 
	 $routeProvider.when('/globalRule' , {
		 templateUrl:'html/ruleglobalrule.html',
		 controller:'globalPgCntrl'
	 }).when('/ruleMaping' , {
		 templateUrl:'html/rulerulemapping.html',
		 controller:'rulePgCntrl'
	 }).otherwise({
		redirectTo:'/globalRule'
	 });

 }]); 
 
 pteRouter.directive('fileModel', ['$parse', function ($parse) {
     return {
        restrict: 'A',
        link: function(scope, element, attrs) {
           var model = $parse(attrs.fileModel);
           var modelSetter = model.assign;
           
           element.bind('change', function(){
              scope.$apply(function(){
                 modelSetter(scope, element[0].files[0]);
              });
           });
        }
     };
  }]);

  
    
   
    

 
 

 

 

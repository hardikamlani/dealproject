var conObj =angular.module('PTMClientApp.Constants',[]);


conObj.constant('ServiceURL',{
//Communication Management Service URLs
 'BASEURL' : '..', // change to .. for Local // Change to  http://lnxdevvm623:8280 for deploymnt
	
 'ALLADHOCMSG_URL':'/webapi/api/ptm/message/readAllAdhocMsg',
 'ALLPSNGRMSG_URL':'/webapi/api/ptm/message/readAllPassengerMsgs',
 'SAVEADHOCMSG_URL':'/webapi/api/ptm/message/saveAdhocMessage',
 'SEARCHPSNGR_URL':'/webapi/api/passenger/data/searchList',
 'STORESENTMSG_URL':'/webapi/api/ptm/message/storeSentMessage', 
 'ALLSTN_URL':'/webapi/api/ptm/station/getAllStations',

 'GLOBAL_SETN_URL':'/webapi/api/ptm/lovlist/readallSettings',
 'SRCHEMPLOYID_URL':'/webapi/api/ptm/user/getEmployeebyId',
 'SRCHEMPLOYNAME_URL':'/webapi/api/ptm/user/getEmployeebyName',
 'SRCHEMPLOYIDNAME_URL' :'/webapi/api/ptm/user/getEmployeebyIdName',
 'SEARCHADHOCMSG_URL':'/webapi/api/ptm/message/searchAdhocMsgs', 
 'SEARCHPSNGRMSG_URL':'/webapi/api/ptm/message/searchPsngrMsgs',
 'ALLFLIGHTS_URL':'/webapi/api/ptm/station/getAllFlights',

 
 //Rule Management Service URLs
 'ALLSTATION_URL':'/webapi/api/ptm/station/allStations',
 'ALLRULES_URL':'/webapi/api/ptm/rules/allRules',
 'MAPTOSTN_URL':'/webapi/api/ptm/station/mapRultoStat',
 'GLOBALRULE_URL':'/webapi/api/ptm/rules/setGlobalRuleSetn',
 'ACTIVRULS_URL' :'/webapi/api/ptm/rules/getAllActiveRules',
 'GLOBALSETN_URL' : '/webapi/api/ptm/lovlist/changeGlobalSettings',
 'LOVBYPARM_URL' :'/webapi/api/ptm/lovlist/retrieve/lov/typeobj',
 'LOVBYPARM_URL_RC' : '/webapi/api/ptm/lovlist/retrieve/lov/type',
	 
  //User Management Service URLs
 'USRGRP_URL':'/webapi/api/ptm/usergroup/readUsergroups',
 'PERMISSIONS_URL':'/webapi/api/ptm/usergroup/readAllPermissions',
 'GRPPERMISSION_URL':'/webapi/api/ptm/usergroup/readUserGrouppermission',
 'CREATEUSR_URL':'/webapi/api/ptm/usergroup/createUserGroup',
 'MAPPERMISSION_URL':'/webapi/api/ptm/usergroup/mapUserPermissiontogroup',
 'SAVEUSR_URL':'/webapi/api/ptm/user/savenewUser',
 'DEPTMNT_URL':'/webapi/api/ptm/user/loadAllDepartments',
 'USRS_URL':'/webapi/api/ptm/user/readUsers',
 'DELETEUSR_URL':'/webapi/api/ptm/user/deleteUser',
 'SEARCHUSR_URL':'/webapi/api/ptm/user/searchUser',
 'DELTUSRGRP_URL':'/webapi/api/ptm/usergroup/deletUsergrp',
  'LOGDINUSR' :'/webapi/api/ptm/user/getUserfrmSecurity',
  'LOGOUTUSR' : '/webapi/api/logout',
  'VALIDTUSRGRP_NM' :'/webapi/api/ptm/usergroup/validateUsergrp',
  'READURSGRPINFO':'/webapi/api/ptm/usergroup/readUsergroupsInfo',
 
  //Menu Control URLs
  'READSUBMENULIST_URL':'/webapi/api/ptm/menu/submenuList',
  'READMSTEMENULIST_URL':'/webapi/api/ptm/menu/masterMenuList'	  

});











/**
* This File contains REST api service calls for all controllers
*/
var fctrtObj=angular.module('DealApp.services', []);

// REST API service calls for rule screen
fctrtObj.factory('RuleConfigServices',function($http){
                
                var retRCObj={};
                // service call to get list of stations
                retRCObj.listAllStations = function(){
                                return $http({method: 'GET', url:""});
                };
                
               
              //service Call to modify the global LOV Settings.
                retRCObj.uploadFile = function(file){
                	 var fd = new FormData();
                     fd.append('file', file);
                			
					
return $http({method:'POST' , url:"http://"+window.location.host+"/webapi/api/couriernetwork/user/insertdealsfile" ,data:fd ,headers:{'Content-Type': undefined} })					
								
                };
                
                
             // service call to get list of search panel
                retRCObj.getLOVByParam = function(parmInput){
                                return $http({method: 'GET', url: "",params:{ paramType : parmInput } });
                };
                
                retRCObj.getlistofFiles = function(){
                    return $http({method: 'GET', url: "http://"+window.location.host+"/webapi/api/couriernetwork/user/getlistoffiles" });
                };
                
                retRCObj.getCountofDealsbyFileName = function(inputParam){
                	
               				
							return $http({method:'POST' , url:"http://"+window.location.host+"/webapi/api/couriernetwork/user/getCountofDealsbyFileName" ,params:{ fileName : inputParam } })
                }
                
                return retRCObj;
               });







# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for inserting the deal records ( abount 1 lac in mongo Db in least possible time)
It provides the file upload client portal 

AS per the tested files , it was possible to insert the file with (1L recods in almost 2 secs )

Client can be accessed using :- http://localhost:8080/client

### How do I get set up? ###

maven (3.0.5) required ,
mongoDB setup required 
JBoss 6 or above required 

1)please run "mvn install" on dealProject-dao
2)please run "mvn install" on dealProject-webapi
3)Please run "mvn install" on dealProject-ui

place the generated war files in the server
1) dealProject-webapi.war
2) dealProject-ui.war

MongoDB setup is required :-
Please create a DB by the name courierNetwork

use courierNetwork

create collections by the name

deal_count ,
fileName_table ,
invalid_deals ,
valid_deals

how to create a collection in MongoDB
db.createCollection("deal_count");
db.createCollection("fileName_table");
db.createCollection("invalid_deals");
db.createCollection("valid_deals");

### Contribution guidelines ###

sample services :-
/webapi/api/couriernetwork/user/insertdealsfile - to insert records in the DB
/webapi/api/couriernetwork/user/getCountofDeals - to count all the deals count in DB
/webapi/api/couriernetwork/user/getCountofDealsbyFileName - to get the deals count by filename
/webapi/api/couriernetwork/user/getlistoffiles - to get the list of all the files inserted


### Who do I talk to? ###

in case of any issues :-
hhamlani89@gmail.com